<?php

namespace eezeecommerce\FrontendBundle;

class FrontendEvents
{
    const PRODUCT_PAGE_INITIALISE = "eezeecommerce.aproduct.page.initialise";

    const PRODUCT_PAGE_COMPLETED = "eezeecommerce.product.page.completed";
}