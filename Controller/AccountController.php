<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\UserBundle\Form\UserType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class AccountController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $user = $this->getDoctrine()->getRepository('eezeecommerceUserBundle:User')
            ->find($this->getUser());

        return $this->render(
            'AppBundle:Account:index.html.twig',
            array(
                "user" => $user,
            )
        );
    }

    /**
     * @Route("/settings")
     */
    public function settingsAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository('eezeecommerceUserBundle:User')
            ->find($this->getUser());

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {

        }


        return $this->render(
            'AppBundle:Account:settings.html.twig',
            array(
                "user" => $user,
                "form" => $form->createView(),
            )
        );
    }

    /**
     * @param $id
     * @Route("/invoice/{id}", requirements={"id"="\d+"})
     */
    public function viewInvoice($id)
    {
        $user = $this->getUser();

        $orders = $this->getDoctrine()
            ->getRepository('eezeecommerceOrderBundle:Orders')
            ->find($id);

        $settings = $this->get("eezeecommerce_settings.setting");

        if (null == ($orders)) {
            throw new AccessDeniedHttpException("You do not have permission to view this invoice");
        }

        if ($orders->getUser() == $user) {
            return $this->render(
                "AppBundle:Admin:invoice.html.twig",
                ["orders" => $orders, "settings" => $settings->loadSettingsBySite()]
            );
        } else {
            throw new AccessDeniedHttpException("You do not have permission to view this invoice");
        }
    }
}
