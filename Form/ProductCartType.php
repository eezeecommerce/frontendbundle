<?php

namespace eezeecommerce\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\UploadBundle\Form\FilesType;
use eezeecommerce\WebBundle\Form\UriType;
use eezeecommerce\ProductBundle\Entity\Product;

class ProductCartType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('base_price', 'money', array(
                    "label" => false,
                    "currency" => "GBP"
                ))
               
                ->add('variants', "entity", array(
                    "label" => false,
                    'class' => "eezeecommerceProductBundle:Variants",
                    'property' => 'stock_code',
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true
                ))
        ; 
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_frontendbundle_productcart';
    }

}
