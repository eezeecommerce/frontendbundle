<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */
namespace eezeecommerce\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;


/**
 * Description of contactUsType
 *
 * @author root
 */
class ContactUsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                null,
                array(
                    "label" => false,
                )
            )
            ->add(
                'email',
                "email",
                array(
                    "label" => false,
                    "constraints" => new Email(),
                )
            )
            ->add(
                'subject',
                null,
                array(
                    "label" => false,
                )
            )
            ->add(
                'message',
                TextareaType::class,
                array(
                    "label" => false,
                    "attr" => array(
                        "rows" => 5,
                    ),
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_frontendbundle_contactus';
    }
}
