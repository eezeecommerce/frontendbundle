<?php

namespace eezeecommerce\FrontendBundle\Controller;

use eezeecommerce\FrontendBundle\Event\FrontendPageEvent;
use eezeecommerce\FrontendBundle\FrontendEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use eezeecommerce\FrontendBundle\Form\ProductCartType;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\FrontendBundle\Form\ContactUsType;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

class FrontendController extends Controller
{

    /**
     * @Route("/", name="index", options={"sitemap" = true})
     * @return type
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Frontend:index.html.twig');
    }

    /**
     * @Route("/login", name="login", options={"sitemap" = true})
     */
    public function loginAction()
    {
        return $this->render('AppBundle:Frontend:login.html.twig');
    }

    /**
     * @Route("/register/", name="register", options={"sitemap" = true})
     */
    public function registerAction(Request $request)
    {
        $formFactory = $this->get('fos_user.registration.form.factory');
        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('AppBundle:Frontend:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/registration", name="registration", options={"sitemap" = true})
     */
    public function registrationAction()
    {
        return $this->render('AppBundle:Frontend:registration.html.twig');
    }

    /**
     * @Route("/register/trade", name="registrationTrade", options={"sitemap" = true})
     */
    public function registerTradeAction(Request $request)
    {
        $formFactory = $this->get('fos_user.registration.form.factory');
        $userManager = $this->get('fos_user.user_manager');
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }


        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
                FOSUserEvents::REGISTRATION_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );

            return $response;
        }

        return $this->render(
            'AppBundle:Frontend:trade_register.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @Route("/contactus", name="contactus", options={"sitemap" = true})
     */
    public function contactUsAction(Request $request)
    {
        $settings = $this->get("eezeecommerce_settings.setting")->loadSettingsBySite();

        $form = $this->createForm(new ContactUsType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $to = $settings->getBusinessContactEmail();
            $params = array(
                "name" => $form['name']->getData(),
                "email" => $form['email']->getData(),
                "subject" => $form['subject']->getData(),
                "message" => $form['message']->getData(),
            );
            $locale = "en";

            $message = $this->get('lexik_mailer.message_factory')->get('contactus', $to, $params, $locale);

            $this->get('mailer')->send($message);

            $this->addFlash(
                'success',
                "Your email has been sent successfully."
            );
            return $this->redirectToRoute("contactus");
        }

        $settings = $this->get("eezeecommerce_settings.setting")->loadSettingsBySite();

        return $this->render('AppBundle:Frontend:contactus.html.twig', array(
            "form" => $form->createView(),
            "settings" => $settings,
        ));
    }

    /**
     * @Route("/search", name="search")
     *
     */
    public function searchAction(Request $request)
    {
        $route = $request->attributes->get("_route");

        $text = $request->query->get("q");

        if (empty($text) || strlen($text) < 2) {
            return $this->render("AppBundle:Frontend:search.html.twig", array("text" => $text));
        }

        $query = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->searchByText($text);

        if (count($query) < 1) {
            return $this->render("AppBundle:Frontend:search.html.twig", array("text" => $text));
        }

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            21
        );

        $uri = $this->getDoctrine()
            ->getRepository("eezeecommerceWebBundle:Uri")
            ->findOneBySlug($route);

        return $this->render("AppBundle:Frontend:search.html.twig", array("pagination" => $pagination, "uri" => $uri, "text" => $text));
    }

    /**
     * @Route("/products/{uri}", name="product")
     *
     * @todo Enable locale and translations on this.
     * @param type $id
     * @return product array
     */
    public function productAction($uri)
    {
        $dispatcher = $this->get("event_dispatcher");

        $params = array();

        $event = new FrontendPageEvent($params);

        $dispatcher->dispatch(FrontendEvents::PRODUCT_PAGE_INITIALISE, $event);

        $product = $this->getDoctrine()
            ->getRepository('eezeecommerceProductBundle:Product')
            ->findOneByUri($uri);

        if (!$product) {
            throw $this->createNotFoundException("Cannot find any products with code: $uri. Please try again");
        }

        $manager = $this->get("eezeecommerce.pricing.manager");

        $product = $manager->getProductPrice($product);

        $params = $event->getParams();

        $params["product"] = $product;

        $event->setParams($params);

        $dispatcher->dispatch(FrontendEvents::PRODUCT_PAGE_COMPLETED, $event);

        return $this->render('AppBundle:Frontend:products.html.twig', $event->getParams());
    }

}
