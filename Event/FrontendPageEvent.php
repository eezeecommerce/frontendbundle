<?php

namespace eezeecommerce\FrontendBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class FrontendPageEvent
 * @package eezeecommerce\FrontendBundle\Event
 */
class FrontendPageEvent extends Event
{
    /**
     * @var array
     */
    private $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }
}